plugins {
    id("org.openjfx.javafxplugin")
}

javafx {
    modules = listOf("javafx.controls")
    version = "16"
}

description = "jFreeChart plugin"

dependencies {
    api("org.jfree:org.jfree.svg:5.0.5")
    // https://mvnrepository.com/artifact/org.jfree/org.jfree.chart.fx
    api("org.jfree:org.jfree.chart.fx:2.0.1")


    api(project(":dataforge-plots"))
}