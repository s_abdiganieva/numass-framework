import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.9.20" apply false
    id("org.openjfx.javafxplugin") version "0.1.0" apply false
    id("com.github.johnrengelman.shadow") version "7.1.0" apply false
}

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")

    group = "inr.numass"
    version = "1.1.0"

    repositories {
        mavenCentral()
        maven("https://oss.sonatype.org/content/repositories/snapshots")
    }

    extensions.findByType<KotlinJvmProjectExtension>()?.jvmToolchain(16)


    dependencies {
        add("api", kotlin("reflect"))
        add("api", "org.jetbrains:annotations:24.0.1")
        add("testImplementation", "junit:junit:4.13.2")

        //Spock dependencies. To be removed later
        // https://mvnrepository.com/artifact/org.spockframework/spock-core
        add("testImplementation", "org.spockframework:spock-core:2.3-groovy-4.0")
    }

    tasks {

        withType<Jar> {
            duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        }

        withType<JavaCompile> {
            options.encoding = "UTF-8"
        }


        withType<KotlinCompile> {
            kotlinOptions {
                javaParameters = true
                freeCompilerArgs = freeCompilerArgs + listOf(
                    "-Xjvm-default=all"
                )
            }
        }

        withType<GroovyCompile>{
            val compileKotlinTask = getByName<KotlinCompile>("compileKotlin")
            dependsOn(compileKotlinTask)
            afterEvaluate {
                classpath += files(compileKotlinTask.destinationDirectory)
            }
        }
    }
}