import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    idea
    kotlin("jvm")
    id("com.google.protobuf") version "0.9.4"
}


repositories {
    mavenCentral()
}

dependencies {
    api("com.google.protobuf:protobuf-java:3.25.0")
    api(project(":numass-core:numass-data-api"))
    api(project(":dataforge-storage"))
}

tasks.withType<KotlinCompile> {
    dependsOn(":numass-core:numass-data-proto:generateProto")
}

//sourceSets {
//    create("proto") {
//        proto {
//            srcDir("src/main/proto")
//        }
//    }
//}

protobuf {
    // Configure the protoc executable
    protoc {
        // Download from repositories
        artifact = "com.google.protobuf:protoc:3.25.0"
    }
    generatedFilesBaseDir = "$projectDir/gen"
}

//tasks.getByName("clean").doLast{
//    delete(protobuf.protobuf.generatedFilesBaseDir)
//}